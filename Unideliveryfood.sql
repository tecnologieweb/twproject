-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Giu 06, 2019 alle 22:34
-- Versione del server: 10.1.36-MariaDB
-- Versione PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `deliveryfood`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `codCarrello` int(11) NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `carrello`
--

INSERT INTO `carrello` (`codCarrello`, `email`) VALUES
(11, 'aurora.laghi@studio.unibo.it'),
(12, 'michele.vitiello@studio.unibo.it'),
(13, 'silvia.spinelli@studio.unibo.it'),
(10, 'vanessa.dibiasi@studio.unibo.it');

-- --------------------------------------------------------

--
-- Struttura della tabella `fornitori`
--

CREATE TABLE `fornitori` (
  `piva` bigint(11) NOT NULL,
  `nome` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `categoria` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `indirizzo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `citta` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `linkImg` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `fornitori`
--

INSERT INTO `fornitori` (`piva`, `nome`, `categoria`, `indirizzo`, `citta`, `telefono`, `email`, `password`, `linkImg`) VALUES
(12874993017, 'New Asia', 'Ristorante Asiatico', 'Via Cavalcavia 74', 'Cesena', '0547 27707', 'newasia@outlook.it', 'newasia1234', 'https://source.unsplash.com/4f4YZfDMLeU/300x150'),
(30970540963, 'L\'Angolo della Pizza', 'Pizzeria', 'Via Arturo Carlo Jemolo 110', 'Cesena', '054728540', 'angolodellapizza@outlook.it', 'angolodellapizza1234', 'https://source.unsplash.com/SU1LFoeEUkk/300x150'),
(46002784196, 'Toki Sushi', 'Ristorante Asiatico', 'Via Zuccherificio 215', 'Cesena', '0547072243', 'tokisushi@outlook.it', 'tokisushi1234', 'https://source.unsplash.com/PW8K-W-Kni0/300x150'),
(73302184761, 'Eat Burger', 'Burger', 'Via Europa 647', 'Cesena', '3894607906', 'eatburger@outlook.it', 'eatburger1234', 'https://source.unsplash.com/JHUkH7nZvTQ/300x150');

-- --------------------------------------------------------

--
-- Struttura della tabella `menu`
--

CREATE TABLE `menu` (
  `codice` int(3) NOT NULL,
  `nome` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `ingredienti` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prezzo` decimal(4,2) NOT NULL,
  `disponibile` tinyint(1) NOT NULL,
  `glutenFree` tinyint(1) DEFAULT NULL,
  `piva` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `menu`
--

INSERT INTO `menu` (`codice`, `nome`, `ingredienti`, `prezzo`, `disponibile`, `glutenFree`, `piva`) VALUES
(1, 'Margherita', 'pomodoro, mozzarella', '4.00', 1, 0, 30970540963),
(2, 'Prosciutto cotto', 'pomodoro, mozzarella, prosciutto cotto', '5.00', 1, 0, 30970540963),
(3, 'Wurstel', 'pomodoro, mozzarella, wurstel', '5.00', 1, 0, 30970540963),
(4, 'Americana', 'pomodoro, mozzarella, wurstel, patate fritte', '6.00', 1, 0, 30970540963),
(5, 'Capricciosa', 'pomodoro, mozzarella, prosciutto cotto, funghi, olive, carciofini', '6.50', 1, 0, 30970540963),
(6, 'Napoli', 'pomodoro, mozzarella, acciughe, origano, capperi', '7.00', 1, 0, 30970540963),
(7, 'Spianata', 'sale, rosmarino', '3.50', 1, 0, 30970540963),
(8, 'Acqua naturale 0,5l', NULL, '0.50', 1, 1, 30970540963),
(9, 'Coca cola 0,33l', NULL, '1.00', 1, 1, 30970540963),
(10, 'Anelli di cipolla', NULL, '4.00', 1, 0, 73302184761),
(11, 'Patate fritte', NULL, '4.00', 1, 1, 73302184761),
(12, 'Hot dog', 'pane, wurstel, ketchup, senape', '4.00', 1, 0, 73302184761),
(13, 'Minihamburger nutella', 'pene, nutella', '1.00', 1, 0, 73302184761),
(14, 'Olive ascolana', NULL, '4.00', 1, 0, 73302184761),
(15, 'Pollo aglio', 'pane panda\', pollo, insalata, pomodoro, cipolla, salsa all\'aglio', '8.00', 1, 0, 73302184761),
(16, 'Vegano', 'pane pro-corn, carne vegana, cipolla, cetrioli, insalata, pomodoro, carote, maionese', '8.00', 1, 0, 73302184761),
(17, 'Manzo fumè', 'pane sesamo, manzo, cipolla rossa, insalata, pomodoro, peperoni, caciotta, salsa fume\'', '9.00', 1, 0, 73302184761),
(18, 'Maiale', 'pane sesamo, maiale, radicchio, pomodoro, cipolla, pancetta, caciotta, salsa bbq', '9.00', 1, 0, 73302184761),
(19, 'Acqua naturale 0,5l', NULL, '0.50', 1, 1, 73302184761),
(20, 'Hosomaki sake', 'riso, alga nori, salmone (4pz)', '3.00', 1, 1, 12874993017),
(21, 'Sashimi misto', '10 filetti di pesce misto (salmone, tonno, branzino..)', '12.00', 1, 1, 12874993017),
(22, 'Tartare sake', 'salmone, avocado', '6.00', 1, 1, 12874993017),
(23, 'Tartare maguro', 'tonno, avocado', '7.00', 1, 1, 12874993017),
(24, 'Uramaki yasai maki', 'alga nori, riso, vegetariano (8pz)', '6.00', 1, 1, 12874993017),
(25, 'Uramaki rainbow roll', '', '10.00', 1, 1, 12874993017),
(26, 'Hosomaki sake', 'riso, alga nori, salmone (6pz)', '4.00', 1, 1, 46002784196),
(27, 'Hosomaki tekka', 'riso, alga nori, tonno (6pz)', '4.00', 1, 1, 46002784196),
(28, 'Nigiri sake', 'salmone (2pz)', '1.50', 1, 1, 46002784196),
(29, 'Nigiri maguro', 'tonno (2pz)', '1.50', 1, 1, 46002784196),
(30, 'Nigiri aka ebi', 'gamberi rossi (2pz)', '2.00', 1, 1, 46002784196),
(31, 'Chirasashi sake don', 'riso con 8 filetti di salmone', '10.00', 1, 1, 46002784196),
(32, 'Chirasashi tekka don', 'riso con 8 filetti di tonno ', '12.00', 1, 1, 46002784196),
(33, 'Uramaki sake maki', 'riso, alga nori, salmone, cetriolo, philadelphia, sesamo (8pz)', '7.00', 1, 1, 46002784196),
(34, 'Uramaki miura maki', 'riso, alga nori, salmone cotto, philadelphia (8pz)', '7.00', 1, 1, 46002784196),
(35, 'Fanta 0.33l', '', '0.95', 1, 0, 30970540963);

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche`
--

CREATE TABLE `notifiche` (
  `codNotifica` int(4) NOT NULL,
  `emailDestinatario` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `emailMittente` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `prodotti` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `stato` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visualizzato` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `notifiche`
--

INSERT INTO `notifiche` (`codNotifica`, `emailDestinatario`, `emailMittente`, `prodotti`, `stato`, `visualizzato`) VALUES
(57, 'eatburger@outlook.it', 'aurora.laghi@studio.unibo.it', '1 Patate fritte', 'Consegnato', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `idOrdine` int(4) NOT NULL,
  `codMenu` int(3) NOT NULL,
  `quantita` int(2) NOT NULL,
  `codCarrello` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`idOrdine`, `codMenu`, `quantita`, `codCarrello`) VALUES
(58, 11, 1, 11);

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `cognome` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`email`, `nome`, `cognome`, `password`) VALUES
('aurora.laghi@studio.unibo.it', 'Aurora ', 'Laghi', 'auroralaghi1234'),
('michele.vitiello@studio.unibo.it', 'Michele', 'Vititello', 'michelevitiello1234'),
('silvia.spinelli@studio.unibo.it', 'Silvia', 'Spinelli', 'silviaspinelli1234'),
('vanessa.dibiasi@studio.unibo.it', 'Vanessa ', 'Di Biasi', 'vanessadibiasi1234');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`codCarrello`),
  ADD KEY `emailUtente` (`email`);

--
-- Indici per le tabelle `fornitori`
--
ALTER TABLE `fornitori`
  ADD PRIMARY KEY (`piva`);

--
-- Indici per le tabelle `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`codice`),
  ADD KEY `piva` (`piva`);

--
-- Indici per le tabelle `notifiche`
--
ALTER TABLE `notifiche`
  ADD PRIMARY KEY (`codNotifica`);

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`idOrdine`),
  ADD KEY `codMenu` (`codMenu`),
  ADD KEY `codCarrello` (`codCarrello`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `carrello`
--
ALTER TABLE `carrello`
  MODIFY `codCarrello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT per la tabella `menu`
--
ALTER TABLE `menu`
  MODIFY `codice` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT per la tabella `notifiche`
--
ALTER TABLE `notifiche`
  MODIFY `codNotifica` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `idOrdine` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `carrello`
--
ALTER TABLE `carrello`
  ADD CONSTRAINT `carrello_ibfk_2` FOREIGN KEY (`email`) REFERENCES `utenti` (`email`);

--
-- Limiti per la tabella `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`piva`) REFERENCES `fornitori` (`piva`);

--
-- Limiti per la tabella `ordini`
--
ALTER TABLE `ordini`
  ADD CONSTRAINT `ordini_ibfk_2` FOREIGN KEY (`codMenu`) REFERENCES `menu` (`codice`),
  ADD CONSTRAINT `ordini_ibfk_3` FOREIGN KEY (`codCarrello`) REFERENCES `carrello` (`codCarrello`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
