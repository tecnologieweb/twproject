<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$conn=new mysqli('localhost', 'root', '', 'deliveryfood');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>

  <?php include './navbar.php';?>

  <div class="big-padding">
    <?php
    if($conn->connect_errno){
      ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
      <?php
    }else{
      $fornitore=$_SESSION['email'];
      $sql1="SELECT * from fornitori where email='$fornitore'";
      $result1=$conn->query($sql1);
      $sql2="SELECT ordini.quantita,menu.nome,menu.prezzo,carrello.email from ordini, menu, carrello where ordini.codMenu=menu.codice AND ordini.codCarrello=carrello.codCarrello AND menu.piva=(SELECT piva from fornitori where fornitori.email='$fornitore')";

      $result2=$conn->query($sql2);

      ?>
        <div class="container">
          <div class="row big-padding">
            <div class="col-sm-12">
              <?php
              while($row1=$result1->fetch_assoc()){
              ?>
              <div class="col-sm-12">
                <div class="thumbnail media <?php echo $row1['categoria']; ?>">
                  <img src="<?php echo $row1['linkImg']?>" alt="" class="media-object pull-left">
                  <div class="media-body">
                    <h3><?php echo $row1['nome']?> - <?php echo $row1['categoria']?></h3>
                    <p><?php echo $row1['indirizzo']?>, <?php echo $row1['citta']?></p>
                    <p>Telefono: <?php echo $row1['telefono']?></p>
                    <p>Email: <?php echo $row1['email']?></p>
                  </div>
                </div>
              </div>
              <?php
              }
              ?>
            </div>
          </div>
          <div class="row">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Quantita</th>
                    <th scope="col">Nome Prodotto</th>
                    <th scope="col">Prezzo</th>
                    <th scope="col">Email cliente</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $j=0;
                if ($result2->num_rows>0){
                  while($row = $result2->fetch_assoc()){
                    foreach ($row as $key => $value) {
                      if ($j==0) {
                        ?><tr><?php
                        $j=1;
                      }?>
                      <td class="<?php echo $key; ?>"><?php echo $value; ?></td>
                      <?php
                    }
                    ?></tr><?php
                    $j=0;
                  }//while
                }//if?>
              </tbody>
              </table>
            </div> <!--resp-->
          </div> <!--row-->
        </div> <?php
    }//else ?>
  </div>

  <?php include './footer.php'; ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>
</html>
