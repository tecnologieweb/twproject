<?php
if (session_status() == PHP_SESSION_NONE){
  session_start();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>
<body>
  <?php include './navbar.php'; ?>
  <div class="container big-padding">
    <h1 class="title-center big-padding">Aiuto & Supporto</h1>

    <div class="panel-group big-padding" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              Cos'è UniDeliveryFood?
            </a>
          </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
          <div class="panel-body">
            UniDeliveryFood è una piattaforma nata da UniBo, permette agli studenti che si trovano al nuovo Campus di Cesena di ricevere i migliori prodotti della città in pochi minuti.<br>
            Ti sei dimenticato il pasto a casa o hai un improvviso languorino? Affidati a noi!<br>
            Ordini ciò che desideri in pochi click e te lo consegnermo facendoti risparmiare tempo. Non dovrai muoverti, arriveremo noi da te!
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
              Come posso effettuare un ordine?
            </a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
          <div class="panel-body">
            Ordinare è semplicissimo! Una volta eseguito l'accesso, devi solo:<br>
            1. Scegliere cosa desideri da mangiare, quindi andare sul sito del ristorante dal quale vuoi ordinare.<br>
            2. Aggiungere al carrello tutti i prodotti nelle quantità che vuoi.<br>
            3. Confermare l'ordine utilizzando il metodo di pagamento che preferisci.<br>
            Per sapere che è andato tutto nel modo corretto, riceverai una notifica di conferma e presa in carico dell'ordine. Non devi fare altro!
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingThree">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
              Quanto costa il servizio?
            </a>
          </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
          <div class="panel-body">
            Ogni ordine, oltre al prezzo dei prodotti, ha un costo per la consegna effettuata dal ristorante. Questo costo dipende dal ristorante che scegli.<br>
            Puoi visualizzarlo sul sito di ogni fornitore e nel riepilogo dell'ordine prima di pagare. Può variare da €0 a €3.00.
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingFour">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
              A che ora è attivo il servizio?
            </a>
          </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
          <div class="panel-body">
            Il servizio è attivo nei giorni e negli orari di apertura del nuovo Campus di Cesena, ovvero dalle 9 alle 20 di tutti i giorni, esclusi i giorni festivi.<br>
            Ti consigliamo comunque di controllare gli orari di apertura dei ristoranti da cui desideri ordinare, li puoi consultare velocemente nelle loro pagine.
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingFive">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
              Quali sono i metodi di pagamento?
            </a>
          </h4>
        </div>
        <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
          <div class="panel-body">
            Il pagamento avviene attraverso la piattaforma, è sicuro!<br>
            Accettiamo carte di credito: Visa, MasterCard e American Express... e se non hai una carta con te, puoi anche pagare in contanti al momento della consegna.<br>
            In qualsiasi caso, il fattorino vi chiederà una firma al momento della consegna per una conferma di ordine andato a buon fine.
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingSix">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
              Quanto tempo devo aspettare prima di ricevere l'ordine?
            </a>
          </h4>
        </div>
        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
          <div class="panel-body">
            In fase di compilazione dell'ordine, è possibile inserire un orario specifico per la consegna. Se non viene inserito un orario di riferimento, il ristorante si farà carico dell'ordine non appena possibile.<br>
            Per entrambe le opzioni, riceverai comunque una notifica non appena l'ordine sarà partito. Inoltre, sul sito di ogni ristornante puoi trovare l'indicazione del tempo medio che impiega il fattorino a raggiungere il Campus dal ristorante.
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingSeven">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
              Dove viene consegnato l'ordine?
            </a>
          </h4>
        </div>
        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
          <div class="panel-body">
            Il nuovo Campus di Cesena ha un'area dedicata al servizio UniDeliveryFood, nella zona ristorazione al primo piano.<br>
            Una volta arrivato, il fattorino attenderà nella zona dedicata appena descritta!
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingEight">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
              L'ordine ricevuto è errato o mancante?
            </a>
          </h4>
        </div>
        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
          <div class="panel-body">
            Se hai ricevuto dei prodotti non conformi al tuo ordine oppure il tuo ordine non è arrivato, vai nella sezione contattaci.<br>
            Scrivici per qualisiasi problema e ci impegneremo a rispondere velocemente proponendoti una soluzione.
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingNine">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
              Posso annullare il mio ordine?
            </a>
          </h4>
        </div>
        <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
          <div class="panel-body">
            Hai bisogno di cancellare il tuo ordine? Puoi provvedere alla cancellazione dell'ordine direttamente tu stesso dalla piattaforma, altrimenti puoi contattare telefonicamente il ristorante scelto.<br>
            Se il fattorino è già partito, l'ordine non sarà più annullabile. Puoi inoltre comunicare il tuo annullamento scrivenodoci tramite la sezione contattaci e troveremo una soluzione assieme.
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTen">
          <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
              Ho un codice promo. Come si usa?
            </a>
          </h4>
        </div>
        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
          <div class="panel-body">
            All'interno del tuo profilo si trova la sezione 'codice promozionale', qui troverai l'elenco dei vari codici attivi.<br>
            Per utilizzarlo, basta inserire il codice nella casella apposita al momento della conferma dell'ordine.<br>
            Attenzione! Ogni codice ha un periodo di validità, la scadenza può essere soggetta a modifiche senza preavviso da UniDeliveryFood in base alle esigenze specifiche di campagne promozionali.
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include './footer.php'; ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>
</html>
