<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$_SESSION['type']='admin';
$servername='localhost';
$username='root';
$password='';
$database='deliveryfood';
$conn=new mysqli($servername,$username,$password,$database);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="boardstyle.php">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>UniDeliveryFood</title>
    <link rel="icon" type="image/jpg" href="logo.jpg" />
  </head>

  <body>
    <?php include './navbar.php'; ?>
    <ul class="nav nav-tabs nav-justified big-padding">
        <li class="active"><a data-toggle="tab" href="#overview"><h4>Overview</h4></a></li>
        <li><a data-toggle="tab" href="#users"><h4>Users</h4></a></li>
        <li><a data-toggle="tab" href="#providers"><h4>Providers</h4></a></li>
        <li><a data-toggle="tab" href="#calendar"><h4>Calendar</h4></a></li>
        <li><a data-toggle="tab" href="#maps"><h4>Maps</h4></a></li>
        <li><a data-toggle="tab" href="#statistics"><h4>Statistics</h4></a></li>
    </ul>

    <div class="tab-content">
      <div id="overview" class="tab-pane fade in active">
        <div class="container-fluid">
          <div class="row big-padding">
            <div class="jumbotron jumbotron-pinks">
              <h1>Welcome, Admin!</h1>
              <p>Nothing is impossible. The word itself says “I’m possible!”.</p>
            </div>
          </div>
          <div class="row big-padding">
            <div class="col-xs-6 col-sm-3">
              <div class="thumbnail">
                <p class="number center">
                  <?php
                    if($conn->connect_errno){
                      ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
                      <?php
                    }else{
                      $query="SELECT COUNT(*) c FROM utenti";
                      $result=$conn->query($query);
                      $row=$result->fetch_assoc();
                      echo $row['c'];
                    }
                  ?>
                </p>
                <p class="center">Users</p>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="thumbnail">
                  <p class="number center">
                    <?php
                      if($conn->connect_errno){
                        ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
                        <?php
                      }else{
                        $query="SELECT COUNT(*) c FROM fornitori";
                        $result=$conn->query($query);
                        $row=$result->fetch_assoc();
                        echo $row['c'];
                      }
                    ?>
                  </p>
                  <p class="center">Providers</p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="thumbnail">
                  <p class="number center">
                    <?php
                      if($conn->connect_errno){
                        ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
                        <?php
                      }else{
                        $query="SELECT COUNT(*) c FROM ordini";
                        $result=$conn->query($query);
                        $row=$result->fetch_assoc();
                        echo $row['c'];
                      }
                    ?>
                  </p>
                  <p class="center">Orders</p>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3">
                <div class="thumbnail">
                  <p class="number center">
                    <?php
                      if($conn->connect_errno){
                        ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
                        <?php
                      }else{
                        $sql="SELECT * FROM menu, ordini WHERE menu.codice=ordini.codMenu";
                        $result=$conn->query($sql);
                        $earn=0;
                        $totalOrder=0;
                        while($row = $result->fetch_assoc()){
                            $totalOrder=$row["prezzo"]*$row["quantita"];
                            $earn+=$totalOrder;
                        }
                        echo $earn;
                      }
                    ?>
                  </p>
                  <p class="center">Earned</p>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div id="users" class="tab-pane fade">
        <div class="container-fluid big-padding">
          <div class="row big-padding">
            <div class="col-xs-4 col-xs-offset-4 col-sm-3 col-sm-offset-1">
              <div class="thumbnail">
                <p class="number center">
                  <?php
                    if($conn->connect_errno){
                      ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
                      <?php
                    }else{
                      $query="SELECT COUNT(*) c FROM utenti";
                      $result=$conn->query($query);
                      $row=$result->fetch_assoc();
                      echo $row['c'];
                    }
                  ?>
                </p>
                <p class="center">Users</p>
              </div>
            </div>
          </div>
        </div>
        <div class="container big-padding table-responsive">
          <?php
            if($conn->connect_errno){
              ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
              <?php
            }else{
              $query="SELECT nome, cognome, email FROM utenti";
              $result=$conn->query($query);
              ?>
              <table class="table table-hover">
                <caption>Table of our users</caption>
                <thead>
                  <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Cognome</th>
                    <th scope="col">Email</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $j=0;
                if ($result->num_rows >0 ){
                  while($row = $result->fetch_assoc()){
                    foreach ($row as $key => $value) {
                      if ($j==0) {
                        ?><tr><?php
                        $j=1;
                      }?>
                      <td class="<?php echo $key; ?>"><?php echo $value; ?></td>
                      <?php
                    }
                    ?></tr><?php
                    $j=0;
                  }//while
                }//if?>
              </tbody>
              </table><?php
            }//else ?>
        </div>
      </div>
      <div id="providers" class="tab-pane fade">
        <div class="container-fluid big-padding">
          <div class="row big-padding">
            <div class="col-xs-4 col-xs-offset-4 col-sm-3 col-sm-offset-1">
              <div class="thumbnail">
                <p class="number center">
                  <?php
                    if($conn->connect_errno){
                      ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
                      <?php
                    }else{
                      $query="SELECT COUNT(*) c FROM fornitori";
                      $result=$conn->query($query);
                      $row=$result->fetch_assoc();
                      echo $row['c'];
                    }
                  ?>
                </p>
                <p class="center">Providers</p>
              </div>
            </div>
          </div>
        </div>
        <div class="container big-padding table-responsive">
          <?php
            if($conn->connect_errno){
              ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
              <?php
            }else{
              $query="SELECT nome, categoria, indirizzo, email, piva FROM fornitori";
              $result=$conn->query($query)
              ?>
              <table class="table table-hover">
                <caption>Table of our providers</caption>
                <thead>
                  <tr>
                    <th scope="col">Nome</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Indirizzo</th>
                    <th scope="col">Email</th>
                    <th scope="col">Partita IVA</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $j=0;
                if ($result->num_rows >0 ){
                  while($row = $result->fetch_assoc()){
                    foreach ($row as $key => $value) {
                      if ($j==0) {
                        ?><tr><?php
                        $j=1;
                      }?>
                      <td class="<?php echo $key; ?>"><?php echo $value; ?></td>
                      <?php
                    }
                    ?></tr><?php
                    $j=0;
                  }//while
                }//if?>
              </tbody>
              </table><?php
            }//else ?>
        </div>
      </div>
      <div id="calendar" class="tab-pane fade">
        <div class="google-calendar big-padding">
          <iframe src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;src=it.italian%23holiday%40group.v.calendar.google.com&amp;color=%23125A12&amp;ctz=Europe%2FRome" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
        </div>
      </div>
      <div id="maps" class="tab-pane fade">
      <div class="google-maps big-padding">
        <div class="container">
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11451.475931556379!2d12.235797!3d44.147946!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xfd40764c879cbbac!2sUniversit%C3%A0+di+Bologna+-+Campus+di+Cesena!5e0!3m2!1sit!2sit!4v1549668970278" width="400" weight="300" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
      </div>
      <div id="statistics" class="tab-pane fade">
        <div class="container-fluid big-padding">
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <canvas id="pie-chart" width="800" height="450"></canvas>
            </div>
            <div class="col-sm-12 col-md-6">
              <canvas id="bar-chart" width="800" height="450"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>

    <button onclick="topFunction()" id="myBtn" title="Go to top">
      <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
    </button>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      // When the user scrolls down 500px from the top of the document, show the button
      window.onscroll = function() {scrollFunction()};
      function scrollFunction() {
        if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
          document.getElementById("myBtn").style.display = "block";
        } else {
          document.getElementById("myBtn").style.display = "none";
        }
      }
      // When the user clicks on the button, scroll to the top of the document
      function topFunction() {
      document.body.scrollTop = 0; // For Safari
      document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script>
      new Chart(document.getElementById("pie-chart"), {
          type: 'pie',
          data: {
            labels: ["Pizza", "Ristorante Asiatico", "Burger"],
            datasets: [{
              label: "Category (Providers)",
              backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
              data: [1,2,1]
            }]
          },
          options: {
            title: {
              display: true,
              text: 'Food category of our providers'
            }
          }
      });
    </script>
    <script>
      // Bar chart
      new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
          labels: ["Monday", "Tuesday", "Wensday", "Thursday", "Friday"],
          datasets: [
            {
              label: "Orders in the days of week",
              backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
              data: [1,2,0,2,4]
            }
          ]
        },
        options: {
          legend: { display: false },
          title: {
            display: true,
            text: 'Orders in the days of week'
          }
        }
      });
    </script>
  </body>
</html>
