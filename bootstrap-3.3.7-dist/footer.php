<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
?>
<!--Contatti-->
<footer>
  <div class="container-fluid" id="contacts">
    <div class="row big-padding">
      <div class="col-sm-3"><h3>Contatti</h3></div>
    </div>
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <h4>Unisciti a noi</h4>
        <a href="diventaFornitore.php">Diventa un nostro fornitore</a>
      </div>
      <div class="col-md-3 col-sm-6">
        <h4>Aiuto</h4>
        <a href="FAQ.php">FAQ</a><br>
        <a data-toggle="modal" href="#contattaci">Contattaci</a>
      </div>
      <div class="col-md-3 col-sm-6">
        <h4>Note legali</h4>
        <a href="https://www.iubenda.com/privacy-policy/60276821">Privacy e coookie policy</a>
      </div>
      <div class="col-md-3 col-sm-6">
        <h4>Dove siamo</h4>
        <p>Via dell'Università, 50 - 47521 Cesena (FC)</p>
        <div class="google-maps">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2862.868782657352!2d12.233608515155973!3d44.14795012831559!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132ca4c4ce61a805%3A0xfd40764c879cbbac!2sUniversit%C3%A0+di+Bologna+-+Campus+di+Cesena!5e0!3m2!1sen!2sit!4v1549472075420" width="300" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>
</footer>

<!--Modals-->
<div class="modal fade" id="contattaci">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Contattaci</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="text">Oggetto:</label>
            <textarea class="form-control" rows="1"></textarea>
          </div>
          <div class="form-group">
            <label for="text">Testo:</label>
            <textarea class="form-control" rows="10"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" value="Invio" class="btn btn-primary">Invio</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
