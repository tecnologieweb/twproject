<?php
if(isset($_SESSION['type'])){
  if($_SESSION['type']=='fornitore'){ ?>
    <nav class="navbar navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-example" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand">
            <p>UniDeliveryFood</p>
          </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" role="navigation"  id="navbar-example">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="profiloForn.php">Home</a></li>
            <li><a href="./ordini.php">Ordini</a></li>
            <li><a href="./notificheForn.php">Notifiche</a></li>
            <li><a href="#contacts">Contatti</a></li>
            <li><a href="./logout.php" id="logout">Log out</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    <div class="modal fade" id="contattaci">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Contattaci</h4>
          </div>
          <div class="modal-body">
            <form>
              <div class="form-group">
                <label for="email">Email:</label>
                <input type="text" name="email" class="form-control" id="email">
              </div>
              <div class="form-group">
                <label for="text">Oggetto:</label>
                <textarea class="form-control" rows="1"></textarea>
              </div>
              <div class="form-group">
                <label for="text">Testo:</label>
                <textarea class="form-control" rows="10"></textarea>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="submit" value="Invio" class="btn btn-primary">Invio</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <?php
  }
  elseif ($_SESSION['type']=='utente'){ ?>
    <nav class="navbar navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-example" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand">
            <p>UniDeliveryFood</p>
          </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" role="navigation"  id="navbar-example">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Home</a></li>
            <li><a href="#contacts">Contatti</a></li>
            <li><a href="./notificheCli.php">Notifiche</a></li>
            <li><a href="carrello.php">Carrello</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profilo<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="profiloCli.php">Vedi profilo</a></li>
                <li><a href="./logout.php" id="logout">Log out</a></li>
              </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    <?php
  }
  else if($_SESSION['type']=='admin'){?> <!--admin-->
    <nav class="navbar navbar-fixed-top">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-example" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-brand">
            <p>UniDeliveryFood</p>
          </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" role="navigation"  id="navbar-example">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="./logout.php" id="logout">Log out</a></li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    <?php
  }
} //isset SESSION
else{?>
  <nav class="navbar navbar-fixed-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-example" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <div class="navbar-brand">
          <p>UniDeliveryFood</p>
        </div>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" role="navigation"  id="navbar-example">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="index.php">Home</a></li>
          <li><a href="#contacts">Contatti</a></li>
          <li><a data-toggle="modal" href="#registrati">Registrati</a></li>
          <li><a data-toggle="modal" href="#accedi">Accedi</a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
  </nav>
<?php
} //else ?>

<!--Modals-->
<div class="modal fade" id="accedi">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Accedi a UniDeliveryFood</h4>
      </div>
      <div id="errorMessage"></div>  <!--Stampa errori per entrambi -->
      <div class="modal-body">
        <div class="panel-group" id="accordionAccedi" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="accediCli">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordionAccedi" href="#collapseAccediCli" aria-expanded="false" aria-controls="collapseAccediCli">
                  Cliente
                </a>
              </h4>
            </div>
            <div id="collapseAccediCli" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accediCli">
              <form id="formCli" action="accediCli.php" method="post" onSubmit="return validateClient();">
                <div class="panel-body">
                  <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="emailAccediCli" class="form-control" id="emailAccediCli">
                  </div>
                  <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="passwordAccediCli" class="form-control" id="passwordAccediCli">
                  </div>
                  <div>
                    <input type="checkbox" onclick="myFunctionCli()">Show Password
                  </div>
                  <div>
                    <a data-toggle="modal" href="#recupera">Hai dimenticato le credenziali?</a>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="row">
                    <button type="submit" id="re" class="btn btn-primary">Accedi</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="accediForn">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordionAccedi" href="#collapseAccediForn" aria-expanded="false" aria-controls="collapseAccediForn">
                  Fornitore
                </a>
              </h4>
            </div>
            <div id="collapseAccediForn" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accediForn">
              <form id="formForn" action="accediForn.php" method="post" onSubmit="return validateForn();" >
                <div class="panel-body">
                  <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="emailAccediForn" class="form-control" id="emailAccediForn">
                  </div>
                  <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="passwordAccediForn" class="form-control" id="passwordAccediForn">
                  </div>
                  <div>
                    <input type="checkbox" onclick="myFunctionForn()">Show Password
                  </div>
                  <div>
                    <a data-toggle="modal" href="#recupera">Hai dimenticato le credenziali?</a>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="row">
                    <button type="submit" id="accediFornitore" class="btn btn-primary">Accedi</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="accediAdmin">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordionAccedi" href="#collapseAccediAdmin" aria-expanded="false" aria-controls="collapseAccediAdmin">
                  Amministratore
                </a>
              </h4>
            </div>
            <div id="collapseAccediAdmin" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accediAdmin">
              <form id="formAdmin" action="accediAdmin.php" method="post" onSubmit="return validateAdmin();" >
                <div class="panel-body">
                  <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" name="emailAccediAdmin" class="form-control" id="emailAccediAdmin" >
                  </div>
                  <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" name="passwordAccediAdmin" class="form-control" id="passwordAccediAdmin">
                  </div>
                  <div>
                    <input type="checkbox" onclick="myFunctionAdmin()">Show Password
                  </div>
                  <div>
                    <a data-toggle="modal" href="#recupera">Hai dimenticato le credenziali?</a>
                  </div>
                </div>
                <div class="panel-footer">
                  <div class="row">
                    <button type="submit" id="accediAmministratore" class="btn btn-primary">Accedi</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--Modals-->
<div class="modal fade" id="recupera">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Contattaci</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="text">Oggetto:</label>
            <textarea class="form-control" rows="1"></textarea>
          </div>
          <div class="form-group">
            <label for="text">Testo:</label>
            <textarea class="form-control" rows="10"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="submit" value="Invio" class="btn btn-primary">Invio</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="registrati">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Registrati a UniDeliveryFood</h4>
      </div>
      <div id="errorMessage2"></div>  <!--Stampa errori per entrambi -->
      <div class="modal-body">
        <div class="panel-group" id="accordionReg" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="regCli">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordionReg" href="#collapseRegCli" aria-expanded="false" aria-controls="collapseRegCli">
                  Cliente
                </a>
              </h4>
            </div>
            <div id="collapseRegCli" class="panel-collapse collapse" role="tabpanel" aria-labelledby="regCli">
              <form id="form2" action="registrazioneCli.php" method="post" onSubmit="return registrationClient();">
                <div class="panel-body">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="text">Nome:</label>
                        <input type="text" name="nomeregCli" class="form-control" id="nomeregCli">
                      </div>
                      <div class="col-md-6">
                        <label for="cf">Cognome:</label>
                        <input type="text" name="cognomeregCli" class="form-control" id="cognomeregCli">
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                          <label for="email">Email:</label>
                          <input type="text" name="emailregCli" class="form-control" id="emailregCli">
                        </div>
                        <div class="col-md-6">
                          <label for="email">Conferma Email:</label>
                          <input type="text" name="confEmailregCli" class="form-control" id="confEmailregCli">
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label for="password">Password:</label>
                        <input type="password" name="passwordregCli" class="form-control" id="passwordregCli" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="La password è consigliabile di almeno 8 caratteri alfa-numerici.">
                      </div>
                      <div class="col-md-6">
                        <label for="password">Conferma Password:</label>
                        <input type="password" name="confPasswordregCli" class="form-control" id="confPasswordregCli">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="checkbox" onclick="myFunctionRegCli()">Show Password
                      </div>
                      <div class="col-md-6">
                        <input type="checkbox" onclick="myFunctionRegConfCli()">Show Password
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <button type="submit" id="registraCliente" class="btn btn-primary">Registrati</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                </div>
              </form>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="regForn">
              <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordionReg" href="#collapseRegForn" aria-expanded="false" aria-controls="collapseRegForn">
                  Fornitore
                </a>
              </h4>
            </div>
            <div id="collapseRegForn" class="panel-collapse collapse" role="tabpanel" aria-labelledby="regForn">
              <form id="form3" action="registrazioneForn.php" method="post" onSubmit="return registrationForn();">
                <div class="panel-body">
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="text">Denominazione aziendale:</label>
                        <input type="text" name="nomeregForn" class="form-control" id="nomeregForn">
                      </div>
                      <div class="col-md-6">
                        <label for="piva">Partita IVA:</label>
                        <input type="text" name="pivaregForn" class="form-control" id="pivaregForn">
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                          <label for="email">Email:</label>
                          <input type="text" name="emailregForn" class="form-control" id="emailregForn">
                        </div>
                        <div class="col-md-6">
                          <label for="email">Conferma Email:</label>
                          <input type="text" name="confEmailregForn" class="form-control" id="confEmailregForn">
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label for="password">Password:</label>
                        <input type="password" name="passwordregForn" class="form-control" id="passwordregForn" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="La password è consigliabile di almeno 8 caratteri alfa-numerici.">
                      </div>
                      <div class="col-md-6">
                        <label for="password">Conferma Password:</label>
                        <input type="password" name="confPasswordregForn" class="form-control" id="confPasswordregForn">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <input type="checkbox" onclick="myFunctionRegForn()">Show Password
                      </div>
                      <div class="col-md-6">
                        <input type="checkbox" onclick="myFunctionRegConfForn()">Show Password
                      </div>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <button type="submit" id="registraFornitore" class="btn btn-primary">Registrati</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Controllo Registrati Client -->
<script>
function registrationClient(){
  var emailregC=document.getElementById("emailregCli");
  var pswregC=document.getElementById("passwordregCli");
  var confEmailregC=document.getElementById("confEmailregCli");
  var confPswregC=document.getElementById("confPasswordregCli");
  var nomeregC=document.getElementById("nomeregCli");
  var cognomeregC=document.getElementById("cognomeregCli");
  var errorBox=document.getElementById("errorMessage2");
  var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
  var goodAlertDiv='<div class="alert alert-success alert-dismissible" role="alert">'
  var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

  emailregC.style.border = "1px solid #ccc";
  pswregC.style.border = "1px solid #ccc";
  confEmailregC.style.border = "1px solid #ccc";
  confPswregC.style.border = "1px solid #ccc";
  nomeregC.style.border = "1px solid #ccc";
  cognomeregC.style.border = "1px solid #ccc";

  if(nomeregC.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato il nome!'+"</div>";
    nomeregC.focus();
    nomeregC.style.border = "3px solid #990033";
    return false;
  }
  if(cognomeregC.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato il cognome!'+"</div>";
    cognomeregC.focus();
    cognomeregC.style.border = "3px solid #990033";
    return false;
  }
  if(emailregC.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
    emailregC.focus();
    emailregC.style.border = "3px solid #990033";
    return false;
  }
  if(confEmailregC.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la conferma mail!'+"</div>";
    confEmailregC.focus();
    confEmailregC.style.border = "3px solid #990033";
    return false;
  }
  if(pswregC.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
    pswregC.focus();
    pswregC.style.border = "3px solid #990033";
    return false;
  }
  if(confPswregC.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la conferma password!'+"</div>";
    confPswregC.focus();
    confPswregC.style.border = "3px solid #990033";
    return false;
  }
  if(emailregC.value!=confEmailregC.value){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo conferma email non corrisponde!'+"</div>";
    confEmailregC.value="";
    confEmailregC.focus();
    confEmailregC.style.border = "3px solid #990033";
    return false;
  }
  if(pswregC.value!=confPswregC.value){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo conferma password non corrisponde!'+"</div>";
    confPswregC.value="";
    confPswregC.focus();
    confPswregC.style.border = "3px solid #990033";
    return false;
  }

  var reg=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  if(!reg.test(emailregC.value)){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo mail non è valido!'+"</div>";
    emailregC.focus();
    emailregC.style.border = "3px solid #990033";
    return false;
  }
  return true;
}
</script>

<script>
function registrationForn(){
  var emailregF=document.getElementById("emailregForn");
  var pswregF=document.getElementById("passwordregForn");
  var confEmailregF=document.getElementById("confEmailregForn");
  var confPswregF=document.getElementById("confPasswordregForn");
  var nomeregF=document.getElementById("nomeregForn");
  var pivaregF=document.getElementById("pivaregForn");
  var errorBox=document.getElementById("errorMessage2");
  var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
  var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

  emailregF.style.border = "1px solid #ccc";
  pswregF.style.border = "1px solid #ccc";
  confEmailregF.style.border = "1px solid #ccc";
  confPswregF.style.border = "1px solid #ccc";
  nomeregF.style.border = "1px solid #ccc";
  pivaregF.style.border = "1px solid #ccc";

  if(nomeregF.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la denominazione!'+"</div>";
    nomeregF.focus();
    nomeregF.style.border = "3px solid #990033";
    return false;
  }
  if(pivaregF.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la P.IVA!'+"</div>";
    pivaregF.focus();
    pivaregF.style.border = "3px solid #990033";
    return false;
  }
  if(emailregF.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
    emailregF.focus();
    emailregF.style.border = "3px solid #990033";
    return false;
  }
  if(confEmailregF.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la conferma mail!'+"</div>";
    confEmailregF.focus();
    confEmailregF.style.border = "3px solid #990033";
    return false;
  }
  if(pswregF.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
    pswregF.focus();
    pswregF.style.border = "3px solid #990033";
    return false;
  }
  if(confPswregF.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la conferma password!'+"</div>";
    confPswregF.focus();
    confPswregF.style.border = "3px solid #990033";
    return false;
  }
  if(emailregF.value!=confEmailregF.value){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo conferma email non corrisponde!'+"</div>";
    confEmailregF.value="";
    confEmailregF.focus();
    confEmailregF.style.border = "3px solid #990033";
    return false;
  }
  if(pswregF.value!=confPswregF.value){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo conferma password non corrisponde!'+"</div>";
    confPswregF.value="";
    confPswregF.focus();
    confPswregF.style.border = "3px solid #990033";
    return false;
  }
  return true;
}
</script>

<script>
  function myFunctionCli(){
    var x = document.getElementById("passwordAccediCli");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
<script>
  function myFunctionForn(){
    var x = document.getElementById("passwordAccediForn");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
<script>
  function myFunctionAdmin(){
    var x = document.getElementById("passwordAccediAdmin");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
<script>
  function myFunctionRegCli(){
    var x = document.getElementById("passwordregCli");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
<script>
  function myFunctionRegConfCli(){
    var x = document.getElementById("confPasswordregCli");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
<script>
  function myFunctionRegForn(){
    var x = document.getElementById("passwordregForn");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
<script>
  function myFunctionRegConfForn(){
    var x = document.getElementById("confPasswordregForn");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>
<!--Controllo Accedi Utente-->
<script>
function validateClient(){
  var emailCli=document.getElementById("emailAccediCli");
  var pswCli=document.getElementById("passwordAccediCli");
  var errorBox=document.getElementById("errorMessage");
  var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
  var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

  emailCli.style.border = "1px solid #ccc";
  pswCli.style.border = "1px solid #ccc";

  if(emailCli.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
    emailCli.focus();
    emailCli.style.border = "3px solid #990033";
    return false;
  }
  if(pswCli.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
    pswCli.focus();
    pswCli.style.border = "3px solid #990033";
    return false;
  }
  var reg=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  if(!reg.test(emailCli.value)){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo mail non è valido!'+"</div>";
    emailCli.focus();
    emailCli.style.border = "3px solid #990033";
    return false;
  }
  return true;
}
</script>
<!--Controllo Accedi Fornitore -->
<script>
function validateForn(){
  var emailForn=document.getElementById("emailAccediForn");
  var pswForn=document.getElementById("passwordAccediForn");
  var errorBox=document.getElementById("errorMessage");
  var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
  var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

  emailForn.style.border = "1px solid #ccc";
  pswForn.style.border = "1px solid #ccc";

  if(emailForn.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
    emailForn.focus();
    emailForn.style.border = "3px solid #990033";
    return false;
  }
  if(pswForn.value==""){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
    pswForn.focus();
    pswForn.style.border = "3px solid #990033";
    return false;
  }
  var reg=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  if(!reg.test(emailForn.value)){
    errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo mail non è valido!'+"</div>";
    emailForn.focus();
    emailForn.style.border = "3px solid #990033";
    return false;
  }
  return true;
}
</script>
<script>
  function validateAdmin(){
    var email = document.getElementById("emailAccediAdmin");
    var psw = document.getElementById("passwordAccediAdmin");
    var errorBox=document.getElementById("errorMessage");
    var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
    var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

    email.style.border = "1px solid #ccc";
    psw.style.border = "1px solid #ccc";

    if(email.value==""){
      errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la mail!'+"</div>";
      email.focus();
      email.style.border = "3px solid #990033";
      return false;
    }
    var reg=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(!reg.test(email.value)){
      errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> il campo email non è valido!'+"</div>";
      email.focus();
      email.style.border = "3px solid #990033";
      return false;
    }
    if(psw.value==""){
      errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato la password!'+"</div>";
      psw.focus();
      psw.style.border = "3px solid #990033";
      return false;
    }
    if(email.value=="vanessa.dibiasi@studio.unibo.it" || email.value=="aurora.laghi@studio.unibo.it"){
      if(psw.value=="admin"){
        return true;
      }else{ //psw errate
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> la password è errata!'+"</div>";
        psw.focus();
        psw.style.border = "3px solid #990033";
        return false;
      }
    }else{//email errata
      errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> la email è errata!'+"</div>";
      email.focus();
      email.style.border = "3px solid #990033";
      return false;
    }
  }
</script>
