<?php //home del cliente
if (session_status() == PHP_SESSION_NONE){
  session_start();
}
$conn=new mysqli('localhost', 'root', '', 'deliveryfood');

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>

  <?php include './navbar.php'; ?>

  <div class="big-padding">
    <?php
    if($conn->connect_errno){
      ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
      <?php
    }else{
      $cliente=$_SESSION['email'];
      $sql1="SELECT nome,cognome,email,password FROM utenti WHERE email='$cliente'";
      $result=$conn->query($sql1);
      ?>
      <div class="container">
        <div class="row big-padding">
          <div class="col-sm-12">
            <?php
            while($row=$result->fetch_assoc()){
            ?>
            <div class="col-sm-6 col-sm-offset-3">
              <h2 class="text-center"><?php echo 'Il mio profilo';?></h2>
              <div class="thumbnail">
                  <p class="text-center"><strong>Nome:</strong> <?php echo $row['nome']?></p>
                  <p class="text-center"><strong>Cognome:</strong> <?php echo $row['cognome']?></p>
                  <p class="text-center"><strong>Email:</strong> <?php echo $row['email']?></p>
                  <p class="text-center"><strong>Password:</strong> <?php echo $row['password']?></p>
              </div>
            </div>
            <?php
            }
            ?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="panel-group" role="tablist" aria-multiselectable="true">
              <?php
              $sql2="SELECT * from carrello where email='$cliente'";
              $result2=$conn->query($sql2);
              if ($result2->num_rows>0) {
                while($row2=$result2->fetch_assoc()){
                  $codice=$row2['codCarrello'];
                }
                $sql3="SELECT menu.piva,menu.nome, ordini.quantita, menu.prezzo FROM menu, ordini where ordini.codCarrello='$codice' AND ordini.codMenu=menu.codice";
                $result3=$conn->query($sql3);
                if ($result3->num_rows>0){
                ?>
                <table class="table table-hover">
                  <caption><h3>I miei ordini</h3></caption>
                  <thead>
                    <tr>
                      <th scope="col">Fornitore</th>
                      <th scope="col">Nome piatto</th>
                      <th scope="col">Quantità</th>
                      <th scope="col">Prezzo</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $j=0;

                      while($row3=$result3->fetch_assoc()){
                          foreach ($row3 as $key => $value) {
                            $piva=$row3['piva'];
                            $s2="SELECT nome from fornitori where piva='$piva'";
                            $res2=$conn->query($s2);
                            while($r2=$res2->fetch_assoc()){
                              $nomeForn=$r2['nome'];
                            if($j==0){
                              ?><tr>
                                <?php $j=1;
                            }
                            if ($key=="piva") {?>
                              <td class="<?php echo $key;?>"><?php echo $nomeForn;?></td><?php
                            }else{
                              ?>
                              <td class="<?php echo $key;?>"><?php echo $value ;?></td>
                              <?php
                            }
                            }
                          }
                        ?>
                      </tr>
                      <?php
                      $j=0;
                      }//while?>
                  </tbody>
                </table><?php
              }//if
              } ?>
            </div> <!--panel-group-->
          </div> <!--col-->
        </div> <!--row-->
      </div>
        <?php
      }//else
      ?>
  </div>

  <?php include './footer.php'; ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>
</html>
