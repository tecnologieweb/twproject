<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$conn=new mysqli('localhost', 'root', '', 'deliveryfood');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>

  <?php include './navbar.php'; ?>

  <div class="container">
    <div class="row big-padding">
      <?php
      if($conn->connect_errno){
        ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
        <?php
      }else{
        //se esiste un carrello non concluso
        if (!empty($_SESSION['product']['id'])){?>

          <div class="col-sm-12">
            <div class="bg-info" style="margin-top:50px; padding:10px;">
              <div class="container">
                <h4 style=" line-height:150%">Continua ad aggiungere prodotti al tuo carrello oppure procedi per concludere l'ordine.<br>
                  Ritirare è semplice! Conferma l'ordine e attendi l'arrivo del fattorino al punto ristoro (Piano Primo, Via dell'Università 50).</h4>
              </div>
            </div>
          </div>

          <div class="col-sm-12">
            <table class="table table-responsive table-hover">
              <caption><h1 style="color:#000">Il tuo carrello</h1></caption>
              <thead>
                <tr>
                  <th scope="col">Quantità</th>
                  <th scope="col">Nome Prodotto</th>
                  <th scope="col">Prezzo</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i=0;
                $totale=0;
                for ($j=0; $j < count($_SESSION['product']['id']) ; $j++) {?>
                  <tr>
                    <td><?php echo $_SESSION['product']['qta'][$i]?></td>
                    <td><?php echo $_SESSION['product']['nome'][$i]?></td>
                    <td><?php $floatprice = number_format($_SESSION['product']['prezzo'][$i],2,',',' '); echo '€ '; echo $floatprice?></td>
                    <td><a href="rimuoviCarrello.php?id=<?php echo $_SESSION['product']['id'][$i]?>"><span class="glyphicon glyphicon-trash"></span></a></td>
                  </tr><?php
                  $totale+= $_SESSION['product']['qta'][$i] * $_SESSION['product']['prezzo'][$i];
                  $i++;
                }
                $floattotal = number_format($totale,2,',',' ');
                ?>
                <tr>
                  <th><h3>Totale carrello</h3></th>
                  <th></th>
                  <th><h3><?php echo '€ '; echo $floattotal ?></h3></th>
                  <th style="vertical-align:inherit;"><a data-toggle="modal" href="#payment"><i style="color: green; font-size:20px" class="glyphicon glyphicon-hand-right"></i></a></th>
                </tr>
              </tbody>
            </table>
          </div>
          <?php
        }else{//non ci sono carrelli a questa mail oppure sono già stati tutti conclusi
          ?>
          <div class="col-sm-12">
            <div class="alert alert-info" role="alert" style="margin-top:50px">
              <div class="modal-body">
                <h3 style="text-align:center">Il tuo carrello è momentaneamente vuoto.</h3>
              </div>
            </div>
          </div>
          <?php
        }//else carrello vuoto
      }//else?>
    </div>
  </div> <!-- continer -->

  <div class="modal fade" id="payment">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <form action="aggiungiOrdine.php" method="post">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Concludi l'ordine</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="text">Inserisci un orario di ritiro:</label>
              <input type="time" min="11:00" max="14:00" step="1800" value="12:00"><br>
              <p>Inserisci un orario non inferiore a 20 minuti a partire da ora al fine di dare la possibilità ai fornitori di preparare il tuo pasto. Diveramente, l'orario di ritiro potrebbe subire ritardi.</p><br>
            </div>
            <div class="form-group">
              <label for="text">Seleziona un metodo di pagamento:</label><br>
              <input type="radio" name="payment" value="contanti" checked> Contanti<br>
              <input type="radio" name="payment" value="credito"> Carta di credito<br>
              <input type="radio" name="payment" value="buono" disabled> Buono pasto
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" value="Invio" class="btn btn-primary">Conferma</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
          </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


  <?php include './footer.php' ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <script type="text/javascript">var nibirumail_advice_text = 'UniDeliveryFood utilizza i cookie per regalarti una navigazione migliore. <a href="javascript:;" class="nibirumail_agreement">Ho capito!</a>';</script>
  <script type="text/javascript" src="https://nibirumail.com/docs/scripts/nibirumail.cookie.min.js"></script>

</body>
</html>
