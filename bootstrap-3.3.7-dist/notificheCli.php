<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$conn=new mysqli('localhost', 'root', '', 'deliveryfood');
$email=$_SESSION['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>

  <?php include './navbar.php'; ?>

  <div class="container">
    <div class="row big-padding">
      <?php
      if($conn->connect_errno){
        ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
        <?php
      }else{
        //se esiste un carrello non concluso
        $sql="SELECT * FROM notifiche WHERE emailMittente='$email' ORDER BY codNotifica DESC";
        $result=$conn->query($sql);
        if ($result->num_rows>0){?>
          <div class="col-sm-12">
            <h3>Lo stato degli ordini che hai effettuato</h3>
            <div class="panel-group big-padding">
              <div class="panel panel-default"><?php

                while($row=$result->fetch_assoc()){
                  $eForn=$row['emailDestinatario'];
                  $s2="SELECT nome from fornitori where email='$eForn'";
                  $re2=$conn->query($s2);
                  while($r2=$re2->fetch_assoc()){
                    $nomeForn=$r2['nome'];
                  }
                  $visual=$row['visualizzato'];?>
                  <?php if($visual==0){?>
                    <div class="panel-body alert-warning ">
                      <div class="row">
                        <div class="col-sm-6">
                          <p><?php echo $nomeForn?></p>
                        </div>
                        <div class="col-sm-6">
                          <p>Non ancora preso in carico</p>
                        </div>
                      </div>
                    </div>
                  <?php } else { ?>
                    <?php if ($row['stato']==NULL) {?>
                      <div class="panel-body ">
                        <div class="row">
                          <div class="col-sm-6">
                            <p><?php echo $nomeForn?></p>
                          </div>
                          <div class="col-sm-4">
                            <p>Preso in carico</p>
                          </div>
                        </div>
                      </div><?php
                    }else{?>
                      <div class="panel-body alert-success">
                        <div class="row">
                          <div class="col-sm-6">
                            <p><?php echo $nomeForn?></p>
                          </div>
                          <div class="col-sm-4">
                            <p><?php echo $row['stato']?></p>
                          </div>
                        </div>
                      </div><?php
                    } ?>
                  <?php }
                }//while?>
              </div>
            </div>
          </div><?php
        }else{//non ci sono carrelli a questa mail oppure sono già stati tutti conclusi
          ?>
          <div class="col-sm-12">
            <div class="alert alert-info" role="alert" style="margin-top:50px">
              <div class="modal-body">
                <h3 style="text-align:center">Non ci sono notifiche.</h3>
              </div>
            </div>
          </div>
          <?php
        }//else carrello vuoto
      }//else?>
    </div>
  </div> <!-- continer -->

  <?php include './footer.php' ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <script type="text/javascript">var nibirumail_advice_text = 'UniDeliveryFood utilizza i cookie per regalarti una navigazione migliore. <a href="javascript:;" class="nibirumail_agreement">Ho capito!</a>';</script>
  <script type="text/javascript" src="https://nibirumail.com/docs/scripts/nibirumail.cookie.min.js"></script>

</body>
</html>
