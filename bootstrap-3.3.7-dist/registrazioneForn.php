<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$conn = new mysqli('localhost','root','','deliveryfood');
$query = "INSERT INTO fornitori (piva,nome,email,password) VALUES (?,?,?,?)";
$st=$conn->stmt_init();
if($st->prepare($query)){
  $st->bind_param('isss',$_POST['pivaregForn'],$_POST['nomeregForn'],$_POST['emailregForn'],$_POST['passwordregForn']);
  $st->execute();
}
$_SESSION['type']='fornitore';
$_SESSION['email']=$_POST['emailregForn'];
header("Location: profiloForn.php"); //fa tornare alla index

?>
