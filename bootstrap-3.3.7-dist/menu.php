<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$conn=new mysqli('localhost', 'root', '', 'deliveryfood');
if(isset($_SESSION['type'])){
  if ($_SESSION['type']=='utente') {
    $act="aggiungiCarrello.php";
  }
}else{
  $act="#";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>

  <?php include './navbar.php'; ?>
  <!-- fornitore cliccato con il menu, aggiungibile al carrello-->

  <div class="big-padding">
    <?php
    if($conn->connect_errno){
      ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
      <?php
    }else{
      $fornitore=$_GET['fornitore'];
      $query="SELECT codice,nome,ingredienti,prezzo FROM menu WHERE piva='$fornitore'";
      $result=$conn->query($query);
      if ($result->num_rows >0 ){?>
        <div class="container">
          <div class="row big-padding">
            <div class="col-sm-12">
              <?php
              $sql1="SELECT * FROM fornitori WHERE piva='$fornitore'";
              $result1=$conn->query($sql1);
              while($row1=$result1->fetch_assoc()){
              ?>
              <div class="col-sm-12">
                <div class="thumbnail media <?php echo $row1['categoria']; ?>">
                  <img src="<?php echo $row1['linkImg']?>" alt="" class="media-object pull-left">
                  <div class="media-body">
                    <h3><?php echo $row1['nome']?> - <?php echo $row1['categoria']?></h3>
                    <p><?php echo $row1['indirizzo']?>, <?php echo $row1['citta']?></p>
                    <p>Telefono: <?php echo $row1['telefono']?></p>
                    <p>Email: <?php echo $row1['email']?></p>
                  </div>
                </div>
              </div>
              <?php
              }
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="panel-group" id="menu" role="tablist" aria-multiselectable="true">
                <?php if($_SERVER['REQUEST_METHOD']=='POST' || $_SESSION['newelement']==1){
                  if (isset($_SESSION['type'])) {
                    if ($_SESSION['type']=='utente') {
                      ?><div class="alert alert-success alert-dismissible text-center" role="alert">
                        <h4>Il prodotto è stato aggiunto al tuo carrello, continua i tuoi ordini.</h4></div><?php
                        $_SESSION['newelement']=0;
                    }
                  }else{
                    ?><div class="alert alert-danger alert-dismissible text-center" role="alert">
                      <h4>Accedi o registrati. Riempi il carrello e ricevi velocemte il tuo ordine.</h4></div><?php
                  }
                }?>
                <h3><?php echo 'Il nostro menu';?></h3><?php
                while($row = $result->fetch_assoc()){
                  $codice=$row['codice'];
                  $nome=$row['nome'];
                  $ingredienti=$row['ingredienti'];
                  $prezzo=$row['prezzo'];
                  ?>
                  <div class="panel panel-default">
                    <div class="panel-heading"><h4><?php echo $nome ?></h4>
                    </div>
                    <div id="c<?php echo $codice?>">
                      <div class="panel-body">
                        <div class="container">
                          <div class="row">
                            <div class="col-xs-12 col-sm-6">
                              <h5><?php echo $ingredienti?></h5>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                              <h5><?php echo '€ '; echo $prezzo?></h5>
                            </div>
                            <form action="<?php echo $act ?>" method="post">
                            <div class="col-xs-6 col-sm-3">
                              <input type="hidden" name="id" value="<?php echo $codice?>">
                              <input type="hidden" name="qta" value="1">
                              <input type="hidden" name="nome" value="<?php echo $nome ?>">
                              <input type="hidden" name="prezzo" value="<?php echo $prezzo?>">
                              <button type="submit" class="btn btn-sm btn-aggCarrello">Aggiungi al carrello</button>
                            </div>
                            </form>
                          </div><!--row-->
                        </div>
                      </div>
                    </div>
                  </div><?php
                }//while
              ?>  </div> <!--panel-group-->
            </div>
          </div> <!--row-->
        </div> <?php //container
      }//if
    }//else ?>
  </div>

  <?php include './footer.php' ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>
</html>
