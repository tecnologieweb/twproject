<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}

$conn=new mysqli('localhost', 'root', '', 'deliveryfood');
if(isset($_GET['categoria'])){
  $categoria=$_GET['categoria'];
}else{
  $categoria='*';
}
if (!isset($_SESSION['type'])) {
  $_SESSION['newelement']=0;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>
  <?php include './navbar.php'; ?>
  <!--Carousel-->
  <div id="home" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#home" data-slide-to="0" class="active"></li>
      <li data-target="#home" data-slide-to="1"></li>
      <li data-target="#home" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active" style="background-image: url(https://source.unsplash.com/collection/140489/);">
      </div>
      <div class="item" style="background-image: url(https://source.unsplash.com/collection/191435/);">
      </div>
      <div class="item" style="background-image: url(https://source.unsplash.com/collection/1544860/);">
      </div>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#home" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#home" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>


  <!--thumbnails fornitori-->
  <div class="container">
    <div class="row big-padding">
      <div class="col-sm-3"><h3>I nostri fornitori</h3></div>
    </div>
    <div class="row">
      <!-- Control buttons -->
      <div id="myBtnContainer">
        <button class="btn filter active" onclick="filterSelection('all')">Show all</button>
        <?php
        $query="select distinct categoria from fornitori";
        $result=$conn->query($query);
        while ($row=$result->fetch_assoc()) {
          ?>
          <button class="btn filter" onclick="filterSelection('<?php echo $row['categoria'];?>')"><?php echo $row['categoria'];?></button>
          <?php
        }
        ?>
      </div>
    </div>
    <div class="row big-padding">
      <?php
      if($categoria=='*'){
        $sql="select * from fornitori";
      }
      $result=$conn->query($sql);
      while($row=$result->fetch_assoc()){
      ?>
      <div class="col-xs-6 col-sm-4 col-md-3">
        <a href="menu.php?fornitore= <?php echo $row['piva']?>"><div class="thumbnail filterDiv <?php echo $row['categoria']; ?>">
          <img src="<?php echo $row['linkImg']?>">
          <div class="caption"><h5><?php echo $row['nome']?></h5>
          <p><?php echo $row['categoria']?></p></div>
        </div></a>
      </div>
      <?php
      }
      ?>
    </div>
  </div>

  <button onclick="topFunction()" id="myBtn" title="Go to top">
    <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
  </button>

  <?php include './footer.php'; ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $(function () {
      $('[data-toggle="popover"]').popover()
    });
  </script>
  <script>
    // When the user scrolls down 500px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
      if (document.body.scrollTop > 500 || document.documentElement.scrollTop > 500) {
        document.getElementById("myBtn").style.display = "block";
      } else {
        document.getElementById("myBtn").style.display = "none";
      }
    }
    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    }
  </script>
  <script>
  filterSelection("all")
  function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filterDiv");
  if (c == "all") c = "";
  // Add the "show" class (display:block) to the filtered elements, and remove the "show" class from the elements that are not selected
  for (i = 0; i < x.length; i++) {
    w3RemoveClass(x[i], "show");
    if (x[i].className.indexOf(c) > -1) w3AddClass(x[i], "show");
  }
  }

  // Show filtered elements
  function w3AddClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
  }
  // Hide elements that are not selected
  function w3RemoveClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
  }

  // Add active class to the current control button (highlight it)
  var btnContainer = document.getElementById("myBtnContainer");
  var btns = btnContainer.getElementsByClassName("btn");
  for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function() {
      var current = document.getElementsByClassName("active");
      current[0].className = current[0].className.replace(" active", "");
      this.className += " active";
    });
  }
  </script>
  <script type="text/javascript">var nibirumail_advice_text = 'UniDeliveryFood utilizza i cookie per regalarti una navigazione migliore. <a href="javascript:;" class="nibirumail_agreement">Ho capito!</a>';</script>
  <script type="text/javascript" src="https://nibirumail.com/docs/scripts/nibirumail.cookie.min.js"></script>
</body>
</html>
