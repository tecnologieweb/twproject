<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>

  <?php include "./navbar.php"; ?>

  <div class="container-fluid big-padding">
    <div class="row">
      <img width="100%" height="100%" src="campus2.jpeg" alt="campus universitario di Cesena"/>
    </div>
    <div class="row big-padding">
      <div class="col-sm-12 col-md-8 col-md-offset-2">
        <h2 align="center"><b>Stiamo creando qualcosa di eccezionale!</b></h2><br>
        <h4 style='text-align:justify'>Il nostro Team ce la mette tutta a trovare innovazione e creatività. Vogliamo creare un sito che possa unire tutti con la massima semplicità e velocità.<br><br>
          UniDeliveryFood è una start up finanziata da UniBo, opera sul territorio cesenate con il supporto di fornitori indipendenti che garantiscono il servizio di consegna dei loro prodotti presso il nuovo campus universitario nella città di Cesena.<br><br>
          La funzionalità principale della piattaforma permette il collegamento dei clienti direttamente con i fornitori. Chiunque si trovi all'interno della sede universitaria (studenti, professori, personale ATA, ...) può ususfruire di questa via di comunicazione
          per ordinare i prodotti che desidera dai fornitori locali registati al nostro sito.<br><br>
          Se anche tu vuoi collaborare con noi, registrarti direttamente nell'apposita sezione "Registrati". Puoi contattarci all'email vanessa.dibiasi@studio.unibo.it o aurora.laghi@studio.unibo.it se desideri maggiori infomazioni.
        </h4>
      </div>
    </div>
  </div>

  <?php include "footer.php"; ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script>
    $(function () {
      $('[data-toggle="popover"]').popover()
    });
  </script>
</body>
</html>
