<?php
header("Content-type: text/css");
?>
body{
  position: relative;
}
.container {
  overflow: hidden;
}
.filterDiv {
  float: left;
  filter: grayscale(100%);
  background-color: #dcdcdc;
  /*display: none; /* Hidden by default */
}
/* The "show" class is added to the filtered elements */
.show {
  filter: grayscale(0%);
  background-color: #fff;
}
/* Style the buttons */
.btn.filter {
  border: none;
  outline: none;
  padding: 12px 16px;
  background-color: #bb2e29;
  color: #fff;
  cursor: pointer;
}
.btn.filetr:hover , .btn.filter:focus{
  background-color: #6b0808;
  color: #fff;
}
#myBtn {
  display: none; /* Hidden by default */
  position: fixed; /* Fixed/sticky position */
  bottom: 30px; /* Place the button at the bottom of the page */
  right: 30px; /* Place the button 30px from the right */
  z-index: 99; /* Make sure it does not overlap */
  outline: none; /* Remove outline */
  border: none;
  background-color: #fff; /* Set a background color */
  color: #bb2e29; /* Text color */
  cursor: pointer; /* Add a mouse pointer on hover */
  padding: 10px; /* Some padding */
}
#myBtn:hover {
  background-color: #6b0808; /* Add a dark-grey background on hover */
}
.btn-aggCarrello {
  background-color: white;
  color: #000;
  font-weight: bold;
  border: 2px solid #bb2e29;
  border-radius: 10px;
}
.btn-aggCarrello:hover{
  border: 2px solid #6b0808;
}
.glyphicon-trash{
  color: #bb2e29;
}
.glyphicon-trash:hover{
  color: #6b0808;
}

#myNumber{
  text-align: center;
}
.modal div, .modal div h4{
  color: #000;
}
.navbar , .dropdown-menu{
  background-color: #bb2e29;
}
.navbar-brand p{
  color: #fff;
  font-size: 120%;
}
.navbar a , .dropdown-menu>li>a,
footer div a, footer div a:hover, footer div a:focus{
  color: #fff;
}
.nav>li>a:hover , .dropdown-menu>li>a:hover, .navbar-nav>.open>a:hover ,
.nav>li>a:focus , .dropdown-menu>li>a:focus, .navbar-nav>.open>a:focus ,
.navbar-nav>.open>a {
  color: #fff;
  background-color:  #6b0808;
  text-decoration: underline;
}
.navbar-toggle .icon-bar {
  background-color: #fff;
}
.carousel-inner > .item{
  min-height: 500px;
  background-size: cover;
  background-position: center center;
}
.big-padding{
  padding-top: 50px;
}
.modal div h4, .panel-default>.panel-heading{
  color: #bb2e29;
}
.panel-default>.panel-heading{
  background-color: #fff;
}
.panel-group .panel-heading+.panel.collapse>.list-group,
.panel-group .panel-heading+.panel-collapse>.panel-body{
  border-top:1px solid #fff;
}
.modal-header{
  border-bottom:1px solid #fff;
}
.modal-footer{
  border-top:1px solid #fff;
}
.google-maps {
  position: relative;
  padding-bottom: 75%; // Definisco l'aspect ratio
  height: 0;
  overflow: hidden;
}
footer{
  background-color: #bb2e29;
  color: #fff;
  margin-top: 50px;
  padding: 0px 50px 0px;
}
footer div h4, .caption h5{
  font-weight: bold;
  margin: 20px 0px;
}
.title-center{
  text-align:center;
}
