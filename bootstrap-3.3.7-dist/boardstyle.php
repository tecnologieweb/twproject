<?php
header("Content-type: text/css");
?>
caption {
  padding-top: 8px;
  padding-bottom: 8px;
  color: #000;
  font-size: 20px;
  text-align: left;
}
canvas{
  max-height: 360px;
  max-width: 640px;
}
.notification .badge{
  background: #fff;
  color: #bb2e29;
  font-weight: bold;
}
.navbar, .nav-tabs>li{
  background-color: #bb2e29;
  border-bottom-color: #fff;
}
.navbar-brand p{
  color: #fff;
  font-size: 120%;
}
.navbar a, .nav-tabs>li>a>h4 {
  color: #fff;
}
.nav>li>a:hover , .nav>li>a:focus ,
.navbar-nav>.open>a:hover , .navbar-nav>.open>a:focus ,
.navbar-nav>.open>a, .nav-tabs>li:focus>a, .nav-tabs>li:hover>a {
  color: #fff;
  background-color: #6b0808;
  border-bottom-color: #fff;
}
.nav-tabs>.active>a>h4{
  color: #6b0808;
  font-weight: bold;
}
.nav-tabs{
  padding-top: 15px;
  border-color: 1px solid #6b0808;
}
.nav-tabs>li, .nav-tabs.nav-justified>li>a{
  border-radius: 20px 20px 0 0;
}
.navbar-toggle .icon-bar {
  background-color: #fff;
}
.big-padding{
  margin-top: 50px;
}
.nav-tabs>li>a{
  padding: 0px;
}
@media only screen and (max-width: 768px) {
  .nav-tabs>li, .nav-tabs.nav-justified>li>a{
    border-radius: 10px;
  }
}
.jumbotron{
  background-color: #ea9999;
}
.thumbnail{
  border: 1px solid #bb2e29;
}
.number{
  font-size: 300%;
  font-weight: bold;
}
.center{
  text-align: center;
}
.google-maps {
    position: relative;
    padding-bottom: 50%; // This is the aspect ratio
    height: 0;
    overflow: hidden;
}
.google-maps iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
.google-calendar{
  position: relative;
  height: 0;
  /*width: 50%;*/
  padding-bottom: 50%;
}
.google-calendar iframe{
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
#myBtn {
  display: none; /* Hidden by default */
  position: fixed; /* Fixed/sticky position */
  bottom: 30px; /* Place the button at the bottom of the page */
  right: 30px; /* Place the button 30px from the right */
  z-index: 99; /* Make sure it does not overlap */
  outline: none; /* Remove outline */
  border: none;
  background-color: #fff; /* Set a background color */
  color: #bb2e29; /* Text color */
  cursor: pointer; /* Add a mouse pointer on hover */
  padding: 10px; /* Some padding */
}
#myBtn:hover {
  background-color: #6b0808; /* Add a dark-grey background on hover */
}
