<?php //home del fornitore
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$conn=new mysqli('localhost', 'root', '', 'deliveryfood');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>

  <?php include './navbar.php';?>

  <div class="big-padding">
    <?php
    if($conn->connect_errno){
      ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
      <?php
    }else{
      $fornitore=$_SESSION['email'];
      $sql1="SELECT * from fornitori where email='$fornitore'";
      $result1=$conn->query($sql1);
      $sql2="SELECT * from menu where piva=(SELECT piva FROM fornitori WHERE email=\"$fornitore\")";
      $result2=$conn->query($sql2);
      ?>
        <div class="container">
          <div class="row big-padding">
              <?php
              while($row1=$result1->fetch_assoc()){
              ?>
              <div class="col-sm-12">
                <div class="thumbnail media <?php echo $row1['categoria']; ?>">
                  <img src="<?php echo $row1['linkImg']?>" alt="" class="media-object pull-left">
                  <div class="media-body">
                    <h3><?php echo $row1['nome']?> - <?php echo $row1['categoria']?></h3>
                    <p><?php echo $row1['indirizzo']?>, <?php echo $row1['citta']?></p>
                    <p>Telefono: <?php echo $row1['telefono']?></p>
                    <p>Email: <?php echo $row1['email']?></p>
                  </div>
                </div>
              </div>
              <?php
              }
              ?>
          </div>
          <div class="row">
            <div class="col-xs-10">
              <h3><?php echo 'Il nostro menu';?></h3>
            </div>
            <div class="col-xs-2" style="margin-top: 27px">
              <a data-toggle="modal" href="#aggiungiProdotto" >Aggiungi Prodotto</a>
            </div>
          </div>
          <div class="row">
              <div class="panel-group" role="tablist" aria-multiselectable="true">
                <?php
                $q=0;
                while($row = $result2->fetch_assoc()){
                  $codice=$row['codice'];
                  $nome=$row['nome'];
                  $ingredienti=$row['ingredienti'];
                  $prezzo=$row['prezzo'];
                  ?>
                  <div class="panel panel-default">
                    <div class="panel-heading"><h4><?php echo $nome ?></h4>
                    </div>
                    <div id="c<?php echo $codice?>">
                      <div class="panel-body">
                        <div class="container">
                          <div class="row">
                            <div class="col-xs-10 col-sm-8">
                              <h5><?php echo $ingredienti?></h5>
                            </div>
                            <div class="col-xs-2 col-sm-4">
                              <h5><?php echo '€ '; echo $prezzo?></h5>
                            </div>
                          </div><!--row-->
                        </div>
                      </div>
                    </div>
                  </div><?php
                  $q++;
                }//while
              ?>  </div> <!--panel-group-->
          </div> <!--row-->
        </div> <?php
    }//else ?>
  </div>

  <div class="modal fade" id="aggiungiProdotto">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div id="errorMessage3"></div>
        <form id= "form1" action="aggiungiProdotto.php" method="post" onSubmit="return nuovoProdotto();">
        <div class="modal-body">
            <div class="form-group">
              <label for="text">Nome Prodotto:</label>
              <input type="text" name="nomeProdotto" class="form-control" id="nomeProdotto">
            </div>
            <div class="form-group">
              <label for="text">Ingredienti:</label>
              <input type="text" name="ingredientiProdotto" class="form-control" id="ingredientiProdotto">
            </div>
            <div class="form-group">
              <label for="text">Prezzo:</label>
              <input type="number" min=0.00 max=99.99 step=0.01 name="prezzoProdotto" class="form-control" id="prezzoProdotto">
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" value="Invio" class="btn btn-primary">Invio</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Chiudi</button>
        </div>
      </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <script>
    function nuovoProdotto(){
      var nomeProd=document.getElementById("nomeProdotto");
      var ingredProd=document.getElementById("ingredientiProdotto");
      var prezzoProd=document.getElementById("prezzoProdotto");
      var errorBox=document.getElementById("errorMessage3");
      var alertDiv='<div class="alert alert-danger alert-dismissible" role="alert">';
      var goodAlertDiv='<div class="alert alert-success alert-dismi" role="alert">'
      var alertBtn='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

      nomeProd.style.border = "1px solid #ccc";
      ingredProd.style.border = "1px solid #ccc";
      prezzoProd.style.border = "1px solid #ccc";

      if(nomeProd.value==""){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato il nome!'+"</div>";
        nomeProd.focus();
        nomeProd.style.border = "3px solid #990033";
        return false;
      }
      if(prezzoProd.value==""){
        errorBox.innerHTML=alertDiv+alertBtn+'<strong>Attento</strong> hai dimenticato il prezzo!'+"</div>";
        prezzoProd.focus();
        prezzoProd.style.border = "3px solid #990033";
        return false;
      }
      return true;
    }
  </script>

  <?php include './footer.php'; ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

</body>
</html>
