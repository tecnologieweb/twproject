<?php
if(session_status()==PHP_SESSION_NONE){
  session_start();
}
$conn=new mysqli('localhost', 'root', '', 'deliveryfood');
$email=$_SESSION['email'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="style.php">
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <title>UniDeliveryFood</title>
  <link rel="icon" type="image/jpg" href="logo.jpg" />
</head>

<body>

  <?php include './navbar.php'; ?>

  <div class="container">
    <div class="row big-padding">
      <?php
      if($conn->connect_errno){
        ?><p>Error:<?php echo $conn->connect_errno; ?> <?php echo $conn->connect_error; ?></p>
        <?php
      }else{
        //se esiste un carrello non concluso
        $sql="SELECT * FROM notifiche WHERE emailDestinatario='$email' ORDER BY codNotifica DESC";
        $result=$conn->query($sql);
        if ($result->num_rows>0){?>
          <div class="col-sm-12">
            <h3>Le tue notifiche</h3>
            <div class="panel-group">
              <div class="panel panel-default"><?php
                while($row=$result->fetch_assoc()){
                  $eCli=$row['emailMittente'];
                  $s2="SELECT nome,cognome from utenti where email='$eCli'";
                  $re2=$conn->query($s2);
                  while($r2=$re2->fetch_assoc()){
                    $nomeCli=$r2['nome'];
                    $cognomeCli=$r2['cognome'];
                  }
                  $id=$row['codNotifica'];
                  $visual=$row['visualizzato'];?>
                  <?php if($visual==0){
                    $sql2 = "UPDATE notifiche SET visualizzato=1 WHERE codNotifica='$id'";
                    // Prepare statement
                    $stmt = $conn->prepare($sql2);
                    // execute the query
                    $stmt->execute();?>
                    <div class="panel-body alert-info">
                      <div class="row ">
                        <div class="col-sm-4">
                          <p><?php echo $cognomeCli." ".$nomeCli?></p>
                        </div>
                        <div class="col-sm-4">
                          <p><?php echo $row['prodotti']?></p>
                        </div>
                        <div class="col-sm-4">
                          <a href="consegna.php?cod=<?php echo $id?>">Consegna</a>
                        </div>
                      </div>
                    </div>
                  <?php } else { ?>
                    <?php if ($row['stato']=="Consegnato"){ ?>
                      <div class="panel-body alert-success">
                        <div class="row">
                          <div class="col-sm-4">
                            <p><?php echo $cognomeCli." ".$nomeCli?></p>
                          </div>
                          <div class="col-sm-4">
                            <p><?php echo $row['prodotti']?></p>
                          </div>
                          <div class="col-sm-4">
                            <p><?php echo $row['stato']?></p>
                          </div>
                        </div>
                      </div><?php
                    }else{ ?>
                      <div class="panel-body">
                        <div class="row">
                          <div class="col-sm-4">
                            <p><?php echo $cognomeCli." ".$nomeCli?></p>
                          </div>
                          <div class="col-sm-4">
                            <p><?php echo $row['prodotti']?></p>
                          </div>
                          <div class="col-sm-4">
                            <a href="consegna.php?cod=<?php echo $id?>">Consegna</a>
                          </div>
                        </div>
                      </div><?php
                    }
                  }
                }//while?>
              </div>
            </div>
          </div><?php
        }else{//non ci sono carrelli a questa mail oppure sono già stati tutti conclusi
          ?>
          <div class="col-sm-12">
            <div class="alert alert-info" role="alert" style="margin-top:50px">
              <div class="modal-body">
                <h3 style="text-align:center">Non ci sono notifiche.</h3>
              </div>
            </div>
          </div>
          <?php
        }//else carrello vuoto
      }//else?>
    </div>
  </div> <!-- continer -->

  <?php include './footer.php' ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <script type="text/javascript">var nibirumail_advice_text = 'UniDeliveryFood utilizza i cookie per regalarti una navigazione migliore. <a href="javascript:;" class="nibirumail_agreement">Ho capito!</a>';</script>
  <script type="text/javascript" src="https://nibirumail.com/docs/scripts/nibirumail.cookie.min.js"></script>

</body>
</html>
